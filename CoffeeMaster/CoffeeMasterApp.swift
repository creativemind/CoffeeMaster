//
//  CoffeeMasterApp.swift
//  CoffeeMaster
//
//  Created by Reza on 5/22/1401 AP.
//

import SwiftUI

@main
struct CoffeeMasterApp: App {
    
    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
    
    
    
}
