//
//  Offer.swift
//  CoffeeMaster
//
//  Created by Reza on 5/22/1401 AP.
//

import SwiftUI

struct Offer: View {
    var title = ""
    var description = ""
    
    var body: some View {
        VStack{
            Text(title)
                .padding()
                .font(.title)
            Text(description)
        }
    }
    
}

struct Offer_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            Offer(title: "My offer", description: "This is description")
                .previewLayout(.device)
                .preferredColorScheme(.light)
            .previewInterfaceOrientation(.portraitUpsideDown)
            Offer(title: "My offer", description: "This is description")
                .previewDevice("iPhone SE (3rd generation)")
                .previewLayout(.device)
                .preferredColorScheme(.dark)
                .previewInterfaceOrientation(.portraitUpsideDown)
        }
    }
}
